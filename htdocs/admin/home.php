<?php session_start();
include "koneksi.php";

?>
    <!DOCTYPE  html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Ujian Online -- smk bisis.com</title>
		<script src="jquery-1.5.1.min.js" type="text/javascript"></script>
		<link rel="stylesheet" href="css/main.css" type="text/css" media="screen, projection" />
        <link rel="stylesheet" href="css/tabel.css" type="text/css" media="screen, projection" />
        <link rel="shortcut icon" href="favicon.gif" type="image/x-icon">
		<script src="SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
		<link href="styles/style_admin.css" rel="stylesheet" type="text/css">
		<link href="SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
	</head>
	<body onLoad="document.postform.elements['username'].focus();">
		<div id="outer">
			<div id="header">
				<div id="logo">
					<!-- Place your logo here -->
				</div>
	
				<!-- Sign In Box -->
				
			</div>

			<div id="body-top">
			
				<!-- Main Navigation -->

				<div id="TabbedPanels1" class="TabbedPanels">
          <ul class="TabbedPanelsTabGroup">
            <li class="TabbedPanelsTab" tabindex="0" id="home"><a href="?page=welcome" target="_top">Home</a> </li>
            <li class="TabbedPanelsTab" tabindex="1" name="soal" id="soal"><a href="?page=soal" target="_top">Input Soal</a></li>
			<li class="TabbedPanelsTab" tabindex="2" id="lihat"><a href="?page=view" target="_top">Lihat Soal</a></li>
			<li class="TabbedPanelsTab" tabindex="2" id="inputsiswa"><a href="?page=inputsiswa" target="_top">Data Siswa</a></li>
			<li class="TabbedPanelsTab" tabindex="2" id="lihat"><a href="?page=inputguru" target="_top">Data Guru</a></li>
           <li class="TabbedPanelsTab" tabindex="3"><a href="?page=logout" onclick="return confirm('Apakah Anda yakin akan keluar?')">Logout</a></li>			
          </ul>
          </div>
		  
			</div>
			<img src="images/background.jpg">
		  <script type="text/javascript">
$(document).ready(function() {
var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
            $("input").focus(function(e) {
         $(e.target).css("background-color","silver");
      });
      $("input").blur(function(e) {
         $(e.target).css("background-color","white");
      });
})
</script>
			<div id="body-middle" class="clearfix">
			
				<!-- Main Column -->
				<div id="main-col">
				
					<!-- Nameplate Box -->
					<div id="nameplate-top"></div>
					<div id="nameplate-middle" class="clearfix">
						<h2><font color="#333333"><b><marquee behavior="scroll" scrollamount="5">Selamat Datang di Sistem Informasi UJian - <blink>Silmi</blink></marquee></b></font></h2>
					</div>
					<div id="nameplate-bottom"></div>

					<!-- Main Content -->
					<div id="content-top"></div>
					<div id="content-wrapper">
						<div id="content">
						<?php 
							if(isset($_GET['page'])){
								$page=htmlentities($_GET['page']);
							}else{
								$page="welcome";
							}
							
							$file="$page.php";
							//$cek=strlen($page);
							include ($file);
						?>		
                        </div>
					</div>
					<div id="content-bottom"></div>
					
				</div>
				<!-- End Main Column -->
				<!-- Sidebar Content -->
				<div id="side-col">

					<?php
					if(isset($_SESSION['id_user'])){
						include "status.php";
					}else{
						?>
						<!-- Side Box Begin -->
						<div class="side-box-top"></div>
						<div class="side-box-middle">
						<img src="images/user-online.jpeg" width="200" height="176" border="0" />
						<br />
						<br />
						</div>
				  <div class="side-box-bottom"></div>
						<?php
					}
					?>      
            
		</div>
	</body>
		<img src="images/Logo Bisis.jpg">
	<img src="images/down.jpg" />
	</html>